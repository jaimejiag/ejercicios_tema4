package com.jaime.ejercicios_tema4.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jaime on 30/12/16.
 */

public class WeatherGson {
    @SerializedName("description")
    private String description;
    @SerializedName("icon")
    private String icon;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
