package com.jaime.ejercicios_tema4.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jaime on 30/12/16.
 */

public class ListEco {
    @SerializedName("features")
    private List<Ecopoint> ecopointList;

    public List<Ecopoint> getEcopointList() {
        return ecopointList;
    }

    public void setEcopointList(List<Ecopoint> ecopointList) {
        this.ecopointList = ecopointList;
    }
}
