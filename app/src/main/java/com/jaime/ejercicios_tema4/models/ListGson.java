package com.jaime.ejercicios_tema4.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jaime on 29/12/16.
 */

public class ListGson {
    @SerializedName("dt")
    private long date;
    @SerializedName("temp")
    private Temperature temperature;
    @SerializedName("pressure")
    private double pressure;
    @SerializedName("weather")
    private List<WeatherGson> weather;


    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public List<WeatherGson> getWeather() {
        return weather;
    }

    public void setWeather(List<WeatherGson> weather) {
        this.weather = weather;
    }
}
