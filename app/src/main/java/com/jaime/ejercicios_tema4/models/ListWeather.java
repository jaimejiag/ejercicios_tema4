package com.jaime.ejercicios_tema4.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jaime on 29/12/16.
 */

public class ListWeather {
    @SerializedName("list")
    private java.util.List<ListGson> weatherList;

    public java.util.List<ListGson> getWeatherList() {
        return weatherList;
    }

    public void setWeatherList(java.util.List<ListGson> weatherList) {
        this.weatherList = weatherList;
    }
}
