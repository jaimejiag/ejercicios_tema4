package com.jaime.ejercicios_tema4.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jaime on 30/12/16.
 */

public class Temperature {
    @SerializedName("max")
    private double maxTemp;
    @SerializedName("min")
    private double minTemp;


    public double getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(double maxTemp) {
        this.maxTemp = maxTemp;
    }

    public double getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(double minTemp) {
        this.minTemp = minTemp;
    }
}
