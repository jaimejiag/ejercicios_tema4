package com.jaime.ejercicios_tema4.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jaime on 30/12/16.
 */

public class EcoProperties {
    @SerializedName("elemento")
    private String element;
    @SerializedName("direccion")
    private String adress;
    @SerializedName("latitud")
    private String latitude;
    @SerializedName("longitud")
    private String longitude;


    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
