package com.jaime.ejercicios_tema4.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jaime on 30/12/16.
 */

public class Ecopoint {
    @SerializedName("properties")
    private EcoProperties ecoProperties;

    public EcoProperties getEcoProperties() {
        return ecoProperties;
    }

    public void setEcoProperties(EcoProperties ecoProperties) {
        this.ecoProperties = ecoProperties;
    }
}
