package com.jaime.ejercicios_tema4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Button mBtnEj1;
    private Button mBtnEj2;
    private Button mBtnEj3;
    private Button mBtnEj4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBtnEj1 = (Button) findViewById(R.id.btn_ejercicio1);
        mBtnEj1.setOnClickListener(this);

        mBtnEj2 = (Button) findViewById(R.id.btn_ejercicio2);
        mBtnEj2.setOnClickListener(this);

        mBtnEj3 = (Button) findViewById(R.id.btn_ejercicio3);
        mBtnEj3.setOnClickListener(this);

        mBtnEj4 = (Button) findViewById(R.id.btn_ejercicio4);
        mBtnEj4.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()) {
            case R.id.btn_ejercicio1:
                intent = new Intent(getApplicationContext(), Exercise1Activity.class);
                break;

            case R.id.btn_ejercicio2:
                intent = new Intent(getApplicationContext(), Exercise2Activity.class);
                break;

            case R.id.btn_ejercicio3:
                intent = new Intent(getApplicationContext(), Exercise3Activity.class);
                break;

            case R.id.btn_ejercicio4:
                intent = new Intent(getApplicationContext(), Exercise4Activity.class);
                break;
        }

        if (intent != null)
            startActivity(intent);
    }
}
