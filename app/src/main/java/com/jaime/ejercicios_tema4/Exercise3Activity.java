package com.jaime.ejercicios_tema4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.jaime.ejercicios_tema4.utils.Analysis;
import com.jaime.ejercicios_tema4.utils.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class Exercise3Activity extends AppCompatActivity {
    private static final String URL_API = "https://openexchangerates.org/api/latest.json?app_id=";
    private static final String APP_ID = "751593c733e44bf882f63b195ca2950a";
    private static final String TAG = "MyTag";

    private EditText edtDollars;
    private EditText edtEuros;
    private RadioButton rbtnDollarsTo;
    private RadioButton rbtnEurosTo;
    private Button btnConvert;

    private RequestQueue mRequestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise3);

        edtDollars = (EditText) findViewById(R.id.edt_dollars);
        edtEuros = (EditText) findViewById(R.id.edt_euros);
        rbtnDollarsTo = (RadioButton) findViewById(R.id.rbtn_dollarsTo);
        rbtnEurosTo = (RadioButton) findViewById(R.id.rbtn_eurosTo);
        btnConvert = (Button) findViewById(R.id.btn_convert);
        mRequestQueue = MySingleton.getInstance(this.getApplicationContext()).getRequestQueue();

        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                download();
            }
        });
    }

    private void download() {
        String url = URL_API + APP_ID;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            double exchange = Analysis.analyzeExchange(response);
                            showExchange(exchange);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "VolleyError: " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        // Set the tag on the request.
        jsObjRequest.setTag(TAG);
        // Set retry policy
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        // Add the request to the RequestQueue.
        mRequestQueue.add(jsObjRequest);
    }

    private void showExchange(double value) {
        double dollars = 0.0;
        double euros = 0.0;

        if (rbtnDollarsTo.isChecked()) {
            if (!edtDollars.getText().toString().isEmpty()) {
                dollars = Double.valueOf(edtDollars.getText().toString());
                euros = dollars * value;
                edtEuros.setText(String.format("%.2f", euros));
            } else
                Toast.makeText(this, "El campo de dólares no puede estar vacío", Toast.LENGTH_SHORT).show();
        } else {
            if (!edtEuros.getText().toString().isEmpty()) {
                euros = Double.valueOf(edtEuros.getText().toString());
                dollars = euros / value;
                edtDollars.setText(String.format("%.2f", dollars));
            } else
                Toast.makeText(this, "El campo de euros no puede estar vacío", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }
}
