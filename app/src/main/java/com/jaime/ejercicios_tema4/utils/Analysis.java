package com.jaime.ejercicios_tema4.utils;

import com.jaime.ejercicios_tema4.models.Weather;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jaime on 28/12/16.
 */

public class Analysis {
    public static Weather analyzeWeather(JSONObject rootObj) throws JSONException {
        Weather weather = new Weather();
        JSONObject item;
        JSONArray jsonArray;

        jsonArray = rootObj.getJSONArray("weather");
        item = jsonArray.getJSONObject(0);
        weather.setDescription(item.getString("description"));
        weather.setImageID(item.getString("icon"));

        item = rootObj.getJSONObject("main");
        weather.setTemperature(String.valueOf(item.getDouble("temp")));
        weather.setPressure(String.valueOf(item.getInt("pressure")));
        weather.setHumidity(String.valueOf(item.getInt("humidity")));

        return weather;
    }

    public static double analyzeExchange(JSONObject jObj) throws JSONException {
        JSONObject item;
        double result = 0.0;

        item = jObj.getJSONObject("rates");
        result = item.getDouble("EUR");

        return  result;
    }
}
