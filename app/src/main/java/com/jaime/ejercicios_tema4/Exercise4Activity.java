package com.jaime.ejercicios_tema4;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.jaime.ejercicios_tema4.adapters.EcopointsAdapter;
import com.jaime.ejercicios_tema4.adapters.WeatherAdapter;
import com.jaime.ejercicios_tema4.models.Ecopoint;
import com.jaime.ejercicios_tema4.models.ListEco;
import com.jaime.ejercicios_tema4.models.ListWeather;
import com.jaime.ejercicios_tema4.utils.RestClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class Exercise4Activity extends AppCompatActivity {
    private static final String URL = "http://datosabiertos.malaga.eu/recursos/energia/ecopuntos/ecopuntos.json";

    private ListView lstvEcopoints;
    private ArrayList<Ecopoint> mEcopointsList;
    private ListEco mListEco;
    private EcopointsAdapter adapter;
    private Gson mGson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise4);

        lstvEcopoints = (ListView) findViewById(R.id.lstv_ecopoints);
        mEcopointsList = new ArrayList<>();

        lstvEcopoints.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?q=" +
                        mEcopointsList.get(i).getEcoProperties().getLatitude() + "," +
                        mEcopointsList.get(i).getEcoProperties().getLongitude()));
                startActivity(intent);
            }
        });

        download();
    }

    private void download() {
        final ProgressDialog progress = new ProgressDialog(this);
        RestClient.get(URL, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();

                progress.setProgressStyle(ProgressDialog. STYLE_SPINNER );
                progress.setMessage("Conectando . . .");
                progress.setCancelable(true);
                progress.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    mGson = new Gson();
                    mListEco = mGson.fromJson(String.valueOf(response), ListEco.class);
                    show();
                    progress.dismiss();
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "JsonSyntaxException: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Error: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Error: " + responseString, Toast.LENGTH_LONG).show();
            }
        } );
    }

    private void show() {
        if (mListEco != null) {
            mEcopointsList.clear();
            mEcopointsList.addAll(mListEco.getEcopointList());

            if (adapter == null) {
                adapter = new EcopointsAdapter(this, mEcopointsList);
                lstvEcopoints .setAdapter(adapter);
            }
        } else
            Toast.makeText(getApplicationContext(), "Error al crear la lista", Toast. LENGTH_SHORT ).show();
    }
}
