package com.jaime.ejercicios_tema4.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jaime.ejercicios_tema4.R;
import com.jaime.ejercicios_tema4.models.Ecopoint;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by jaime on 30/12/16.
 */

public class EcopointsAdapter extends ArrayAdapter<Ecopoint> {
    private Context context;

    public EcopointsAdapter(Context context, ArrayList<Ecopoint> objects) {
        super(context, R.layout.ecopoint_item, objects);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        EcopointHolder holder;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.ecopoint_item, null);
            holder = new EcopointHolder();

            holder.element = (TextView) view.findViewById(R.id.txv_element);
            holder.adress = (TextView) view.findViewById(R.id.txv_adress);

            view.setTag(holder);
        } else
            holder = (EcopointHolder) view.getTag();

        holder.element.setText(getItem(position).getEcoProperties().getElement());
        holder.adress.setText(getItem(position).getEcoProperties().getAdress());

        return view;
    }

    class EcopointHolder {
        TextView element;
        TextView adress;
    }
}
