package com.jaime.ejercicios_tema4.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaime.ejercicios_tema4.R;
import com.jaime.ejercicios_tema4.models.ListGson;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by jaime on 29/12/16.
 */

public class WeatherAdapter extends ArrayAdapter<ListGson> {
    private static final String URL_ICON = "http://openweathermap.org/img/w/";
    private Context context;

    public WeatherAdapter(Context context, ArrayList<ListGson> objects) {
        super(context, R.layout.weather_item, objects);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        WeatherHolder holder;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.weather_item, null);
            holder = new WeatherHolder();

            holder.icon = (ImageView) view.findViewById(R.id.imgv_weather_ex2);
            holder.date = (TextView) view.findViewById(R.id.txv_date);
            holder.maxTemp = (TextView) view.findViewById(R.id.txv_maxTemp);
            holder.minTemp = (TextView) view.findViewById(R.id.txv_minTemp);
            holder.pressure = (TextView) view.findViewById(R.id.txv_pressure);
            holder.description = (TextView) view.findViewById(R.id.txv_skyStatus);

            view.setTag(holder);
        } else
            holder = (WeatherHolder) view.getTag();


        Picasso.with(context)
                .load(URL_ICON + getItem(position).getWeather().get(0).getIcon() + ".png")
                .into(holder.icon);

        Date d = new Date(getItem(position).getDate() * 1000);
        String df = new SimpleDateFormat("dd/MM").format(d);

        holder.date.setText(df);
        holder.maxTemp.setText(String.valueOf(getItem(position).getTemperature().getMaxTemp()));
        holder.minTemp.setText(String.valueOf(getItem(position).getTemperature().getMinTemp()));
        holder.pressure.setText(String.valueOf(getItem(position).getPressure()) + "hPa");
        holder.description.setText(getItem(position).getWeather().get(0).getDescription());

        return view;
    }

    class WeatherHolder {
        ImageView icon;
        TextView date;
        TextView maxTemp;
        TextView minTemp;
        TextView pressure;
        TextView description;
    }
}
