package com.jaime.ejercicios_tema4;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.jaime.ejercicios_tema4.adapters.WeatherAdapter;
import com.jaime.ejercicios_tema4.models.ListGson;
import com.jaime.ejercicios_tema4.models.ListWeather;
import com.jaime.ejercicios_tema4.utils.RestClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class Exercise2Activity extends AppCompatActivity {
    private static final String URL_WEATHER = "http://api.openweathermap.org/data/2.5/forecast/daily?q=";
    private static final String APPID = "&appid=4f697e5a08af88948c2ea09becdcf2dd";
    private static final String UNITS = "&units=metric";
    private static final String LANGUAGE = "&lang=es";

    private EditText edtCity;
    private Button btnShow;
    private ListView lstvWeather;

    private ArrayList<ListGson> mList;
    private ListWeather mWeather;
    private Gson mGson;
    private WeatherAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise2);

        edtCity = (EditText) findViewById(R.id.edt_city);
        btnShow = (Button) findViewById(R.id.btn_showWeather);
        lstvWeather = (ListView) findViewById(R.id.lstv_weather);
        mList = new ArrayList<>();

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = URL_WEATHER + edtCity.getText() + UNITS + LANGUAGE + APPID;
                download(url);
            }
        });
    }

    private void download(String web) {
        final ProgressDialog progress = new ProgressDialog(this);
        RestClient.get(web, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                adapter = null;
                lstvWeather.setAdapter(null);
                progress.setProgressStyle(ProgressDialog. STYLE_SPINNER );
                progress.setMessage("Conectando . . .");
                progress.setCancelable(true);
                progress.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    mGson = new Gson();
                    mWeather = mGson.fromJson(String.valueOf(response), ListWeather.class);
                    show();
                    progress.dismiss();
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "JsonSyntaxException: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Error: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Error: " + responseString, Toast.LENGTH_LONG).show();
            }
        } );
    }

    private void show() {
        if (mWeather != null) {
            mList.clear();
            mList.addAll(mWeather.getWeatherList());

            if (adapter == null) {
                adapter = new WeatherAdapter(this, mList);
                lstvWeather.setAdapter(adapter);
            }
        } else
            Toast.makeText(getApplicationContext(), "Error al crear la lista", Toast. LENGTH_SHORT ).show();
    }
}
