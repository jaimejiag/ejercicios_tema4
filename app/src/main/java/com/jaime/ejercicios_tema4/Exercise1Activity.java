package com.jaime.ejercicios_tema4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.jaime.ejercicios_tema4.models.Weather;
import com.jaime.ejercicios_tema4.utils.Analysis;
import com.jaime.ejercicios_tema4.utils.MySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class Exercise1Activity extends AppCompatActivity {
    private static final String APPID = "&appid=4f697e5a08af88948c2ea09becdcf2dd";
    private static final String UNITS = "&units=metric";
    private static final String LANGUAGE = "&lang=es";
    private static final String URL_WEATHER = "http://api.openweathermap.org/data/2.5/weather?id=";
    private static final String URL_IMAGE = "http://openweathermap.org/img/w/";
    private static final String TAG = "MyTag";

    private Spinner spnCities;
    private ImageView imgvWeather;
    private TextView txvWeather;

    private ArrayAdapter<String> mAdapter;
    private String[] mIDs = {"2521710", "2514256", "3117735"};
    private String mCityID;
    private RequestQueue mRequestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise1);

        spnCities = (Spinner) findViewById(R.id.spn_cities);
        imgvWeather = (ImageView) findViewById(R.id.imgv_weather);
        txvWeather = (TextView) findViewById(R.id.txv_weather);
        mRequestQueue = MySingleton.getInstance(this.getApplicationContext()).getRequestQueue();

        mAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.cities)
        );
        spnCities.setAdapter(mAdapter);

        spnCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mCityID = mIDs[i];
                download();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        download();
    }

    private void download() {
        String url = URL_WEATHER + mCityID + UNITS + LANGUAGE + APPID;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Weather weather = Analysis.analyzeWeather(response);

                            Picasso.with(getApplicationContext())
                                    .load(URL_IMAGE + weather.getImageID() + ".png")
                                    .into(imgvWeather);

                            txvWeather.setText("Estado del cielo: " + weather.getDescription() +
                            "\nTemperatura: " + weather.getTemperature() +
                            "\nPresión atmosférica: " + weather.getPressure() +
                            "\nHumedad: " + weather.getHumidity());
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "VolleyError: " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        // Set the tag on the request.
        jsObjRequest.setTag(TAG);
        // Set retry policy
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        // Add the request to the RequestQueue.
        mRequestQueue.add(jsObjRequest);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }
}
