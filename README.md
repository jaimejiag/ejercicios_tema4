### Ejercicio 1 ###
Muestra los datos climáticos actualizados de tres ciudades que se eligen mediante un spinner.
Se usa los IDs de las ciudades que facilita la API para evitar ambigüedades en el resultado.

### Ejercicio 2 ###
Muestra los datos climáticos de los próximos siete días de la ciudad que se introduzca.
Puede que se muestren resultados aunque no se escriba bien la ciudad, se debe a que la API muestra la ciudad con el nombre más parecido.
Debido a la ambigüedad de buscar las ciudades por nombre es posible que los datos climatológicos que se muestren no correspondan con la realidad, ya que puede existir varias ciudades en el mundo con el mismo nombre.

### Ejercicio 3 ###
Convierte dólares a euros y viceversa. El valor que se obtiene de la API es, en euros, el equivalente a un dólar, a partir de ese valor se hacen los cálculos pertinentes.

### Ejercicio 4 ###
Muestra los puntos ecológicos de la ciudad de Málaga en una lista, muestra qué es y en que dirección se encuentra. Cuando se toca un elemento de la lista se mostrará en Google Maps su localización.
Los datos se obtiene de un documento JSON facilitado por **datos.gob.es**, la url exacta está en una constante en la clase **Exercise4Activity** 